import ffmpeg
from utils import time_to_timestamp
import subprocess


def get_audio_duration(filename):
    return float(ffmpeg.probe(filename)["streams"][0]["duration"])


def get_video_duration(filename):
    return float(ffmpeg.probe(filename)["streams"][0]["duration"])


def audio_concat(audio_streams, total_duration=None):
    if len(audio_streams) == 1:
        stream = audio_streams[0]
    else:
        stream = ffmpeg.concat(*audio_streams, v=0, a=1)

    if total_duration is None:
        return stream
    return stream.filter("apad", whole_dur=total_duration)


def video_concat(video_streams):
    if len(video_streams) == 1:
        return video_streams[0]
    return ffmpeg.concat(*video_streams)


def video_concat_keep_audio(video_streams):
    streams = []
    for (video_stream, audio_stream) in video_streams:
        streams.append(video_stream)
        streams.append(audio_stream)
    return ffmpeg.concat(*streams, v=1, a=1)


def combine_video_audio(video_stream, audio_stream):
    return ffmpeg.concat(video_stream, audio_stream, v=1, a=1)


def audio_with_delay(audio_file, delay):
    delay_ms = delay * 1000
    return ffmpeg.input(audio_file).filter("adelay", f"{delay_ms}|{delay_ms}")


PREVIEW_FPS = 5
FPS = 30


def image_to_video(image_file, duration, is_preview):
    frame_rate = PREVIEW_FPS if is_preview else FPS
    return ffmpeg.input(image_file, loop="1", t=time_to_timestamp(duration), framerate=f"{frame_rate}")


def video_clip(video_file):
    return ffmpeg.input(video_file)


def trim_clip(video, start, end):
    return video.trim(start=time_to_timestamp(start), end=time_to_timestamp(end)).setpts("PTS-STARTPTS")


FAST_FPS = 5


def scale_fast(clip):
    return clip.filter("scale", width="-1", height="720")


def write_video_file_fast(clip, output_file):
    stream = clip.filter("scale", width="-1", height="720").output(output_file, vcodec="libx264", acodec="aac", preset="superfast", vsync="2")
    print("ffmpeg", " ".join(stream.get_args()))
    stream.run()


def preview_clip(clip):
    ffmpeg_process = scale_fast(clip).output("pipe:", format="mpegts", preset="superfast", vsync="2").run_async(pipe_stdout=True)
    ffplay_process = subprocess.Popen(
        [
            "ffplay",
            "-i",
            "pipe:",
        ],
        stdin=ffmpeg_process.stdout,
    )
    ffmpeg_process.wait()
    ffplay_process.wait()


def write_video_file(clip, output_file):
    clip.output(output_file, vsync="2", vcodec="libx264", pix_fmt="yuv420p").run()
