import datetime
import time
import pprint

prettyPrinter = pprint.PrettyPrinter(indent=2)


def time_to_timestamp(time):
    return (datetime.datetime(1, 1, 1) + datetime.timedelta(seconds=time)).strftime("%H:%M:%S.%f")[:-3]


def timestamp_filename():
    return time.strftime("%Y-%m-%d--%H%M%S")


def pretty_print(x):
    prettyPrinter.pprint(x)
