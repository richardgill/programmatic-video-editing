import os
from ffmpeg_utils import (
    get_audio_duration,
    audio_with_delay,
    audio_concat,
    image_to_video,
    video_concat,
    combine_video_audio,
    video_concat_keep_audio,
    get_video_duration,
    video_clip,
    preview_clip,
    write_video_file_fast,
)
from video_utils import create_video
from utils import time_to_timestamp, timestamp_filename, pretty_print
import importlib
import time
from webvtt import WebVTT, Caption


class Assets:
    def __init__(self, folder):
        self.folder = folder

    def audio(self, file_path, captions=None):
        return {
            "type": "audio",
            "file": os.path.join(self.folder, "audio", f"{file_path}.mp3"),
            "captions": captions,
        }

    def video(self, file_path):
        return {
            "type": "video",
            "file": os.path.join(self.folder, "videos", f"{file_path}.mp4"),
        }

    def image(self, file_path, duration=0):
        return {
            "type": "image",
            "file": os.path.join(self.folder, "videos", f"{file_path}.png"),
            "duration": duration,
        }


def build_audio(timeline, start_time):
    duration = 0
    previous_gaps = 0
    audio_clips = []
    for audio in timeline["audio"]:
        if audio["type"] == "audio":
            audio_duration = get_audio_duration(audio["file"])
            delay_before = start_time + previous_gaps
            audio_clip = audio_with_delay(audio["file"], delay_before)
            audio_clips.append(audio_clip)
            duration += audio_duration
            previous_gaps = 0
        if audio["type"] == "gap":
            duration += audio["length"]
            previous_gaps += audio["length"]
    return (audio_concat(audio_clips, duration), duration)


def build_video(timeline, start_time, audio_duration, is_preview=False):
    video_clips = []
    for index, video in enumerate(timeline["videos"]):
        clip = None
        duration_until_end_of_audio = 0 if index < len(timeline["videos"]) - 1 else audio_duration - start_time
        if duration_until_end_of_audio < 0:
            raise Exception(f"audio is shorter than videos for timeline: {timeline}")
        if video["type"] == "image":
            image_duration = duration_until_end_of_audio if duration_until_end_of_audio > 0 else video["duration"]
            clip = image_to_video(video["file"], duration=image_duration, is_preview=is_preview)
            start_time += image_duration
        else:
            clip = video_clip(video["file"])
            start_time += get_video_duration(video["file"])
        video_clips.append(clip)
    return video_concat(video_clips)


def build_timeline_part(timeline, start_time=0, is_preview=False):
    (audio_clip, audio_duration) = build_audio(timeline, start_time)
    video_clip = build_video(timeline, start_time, audio_duration, is_preview=is_preview)
    return (video_clip, audio_clip)


def build_timeline(timeline, is_preview=False):
    clips = []
    for t in timeline:
        clip = build_timeline_part(t, is_preview=is_preview)
        clips.append(clip)
    return clips


CAPTION_BUFFER_END = 0.5


def build_caption_part(timeline, start_time):
    captions = []
    duration = 0
    for audio in timeline["audio"]:
        if audio["type"] == "audio":
            audio_clip_duration = get_audio_duration(audio["file"])
            clip_start_time = start_time + duration
            captions.append(
                {
                    "start": clip_start_time,
                    "end": clip_start_time + audio_clip_duration + CAPTION_BUFFER_END,
                    "text": audio["captions"],
                }
            )
            duration += audio_clip_duration
        if audio["type"] == "gap":
            duration += audio["length"]
    return (captions, duration)


def build_captions_for_timeline(timeline, start_time=0):
    vtt = WebVTT()
    captions = []
    for t in timeline:
        (new_captions, duration) = build_caption_part(t, start_time)
        captions.extend(new_captions)
        start_time += duration

    for index, caption in enumerate(captions):
        next_start = captions[index + 1]["start"] if index < len(captions) - 1 else 99999999999
        vtt.captions.append(
            Caption(
                time_to_timestamp(caption["start"]),
                time_to_timestamp(min(caption["end"], next_start)),
                caption["text"],
            )
        )
    return vtt


def timeline_from_py_module(python_module):
    module_name = python_module.replace("src/", "").replace(".py", "").replace("/", ".")
    module = importlib.import_module(module_name)
    return module.timeline()


PREVIEW_FOLDER = "./previews/"
OUTPUT_FOLDER = "./output/"


def generate_file_path(folder):
    return os.path.join(folder, f"{timestamp_filename()}.mp4")


def generate_captions_path(folder):
    return os.path.join(folder, f"{timestamp_filename()}.vtt")


def try_mkdir_folder(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)


def preview_timeline_for_py_module(python_module, fromIndex, toIndex, folder, isWrite):
    captions_file = generate_captions_path(folder)
    timeline = timeline_from_py_module(python_module)
    toIndex = len(timeline) if toIndex == None else toIndex
    fromIndex = 0 if toIndex == None else fromIndex
    timeline = timeline[fromIndex:toIndex]
    print("timeline")
    pretty_print(timeline)
    captions = build_captions_for_timeline(timeline)
    captions.save(captions_file)
    clips = build_timeline(timeline, is_preview=True)
    video = video_concat_keep_audio(clips)
    if isWrite:
        try_mkdir_folder(folder)
        file = generate_file_path(folder)
        write_video_file_fast(video, file)
        os.system(f"open {file}")
    else:
        preview_clip(video)


def write_full_py_module(python_module, folder=OUTPUT_FOLDER):
    try_mkdir_folder(folder)
    file = generate_file_path(folder)
    captions_file = generate_captions_path(folder)
    timeline = timeline_from_py_module(python_module)
    captions_start_time = get_video_duration("./assets/common/Intro.mov")
    captions = build_captions_for_timeline(timeline, captions_start_time)
    captions.save(captions_file)
    create_video(build_timeline(timeline), file)
    os.system(f"open {file}")


def sm_gap():
    return {"type": "gap", "length": 0.5}


def md_gap():
    return {"type": "gap", "length": 0.8}


def lg_gap():
    return {"type": "gap", "length": 1}


def xl_gap():
    return {"type": "gap", "length": 1}


def xxl_gap():
    return {"type": "gap", "length": 1}


def start_video_gap():
    return {"type": "gap", "length": 1}


def start_section_gap():
    return {"type": "gap", "length": 1.3}


def end_section_gap():
    return {"type": "gap", "length": 0.8}
