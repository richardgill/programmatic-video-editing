from editing import (
    Assets,
    start_section_gap,
    end_section_gap,
    sm_gap,
    md_gap,
    lg_gap,
    xl_gap,
    xl_gap,
)


def timeline():
    a = Assets("./assets/javascript/4-strings")
    return [
        {
            "audio": [
                start_section_gap(),
                a.audio("1", ["Welcome back!"]),
                a.audio("2", ["The next type we’ll look at in Javascript is called a ‘string’"]),
                end_section_gap(),
            ],
            "videos": [a.image("0")],
        },
        {
            "audio": [a.audio("38-1", ["We do 3+1 inside the brackets", "which javascript evaluates to 4"]), md_gap()],
            "videos": [a.video("43"), a.video("45"), a.image("46")],
        },
        {
            "audio": [
                a.audio("38-2", ["The whole string says: “my 4 dogs”"]),
                md_gap(),
                a.audio("39", ["You can also use a const you’ve defined above in there. Neat."]),
                end_section_gap(),
            ],
            "videos": [
                a.video("47"),
                a.image("48"),
            ],
        },
        {
            "audio": [start_section_gap(), a.audio("40", ["Lets practice some tasks with strings!"]), end_section_gap()],
            "videos": [a.image("49")],
        },
    ]
