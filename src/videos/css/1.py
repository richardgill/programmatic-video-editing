from editing import build_timeline, Assets, start_video_gap, new_section_gap, sm_gap, md_gap, lg_gap, xl_gap, xl_gap


def timeline():
    a = Assets("./assets/css")
    return [
        {"audio": [start_video_gap(), a.audio("0")], "videos": [a.image("0")]},
        {"audio": [new_section_gap(), a.audio("1")], "videos": [a.image("1")]},
        {"audio": [md_gap(), a.audio("2")], "videos": [a.image("2")]},
        {"audio": [md_gap(), a.audio("3")], "videos": [a.image("3")]},
    ]
