from editing import preview_timeline_for_py_module
import argparse


def main():
    parser = argparse.ArgumentParser(description="Preview timeline")
    parser.add_argument("timeline", help="timeline to preview")
    parser.add_argument("--from", type=int, help="timeline part index to preview", default=None)
    parser.add_argument("--to", type=int, help="timeline part index to preview", default=None)
    parser.add_argument("--output-folder", help="timeline part index to preview", default="./previews/")
    parser.add_argument("--write", type=bool, default=False)
    args = parser.parse_args()
    print(args)
    preview_timeline_for_py_module(args.timeline, getattr(args, "from"), args.to, args.output_folder, args.write)


if __name__ == "__main__":
    main()
