from editing import write_full_py_module
import argparse

def main():
  parser = argparse.ArgumentParser(description='Build timeline')
  parser.add_argument('timeline', help='timeline to build')
  parser.add_argument('--output-folder', help='timeline part index to preview', default='./output/')
  args = parser.parse_args()
  write_full_py_module(args.timeline, folder=args.output_folder)

if __name__ == "__main__":
    main()
