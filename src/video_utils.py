from ffmpeg_utils import video_clip, video_concat_keep_audio, combine_video_audio, video_concat, audio_concat, get_video_duration, write_video_file
import numpy as np
import os
import time
import ffmpeg
import subprocess

watermark_file = "./assets/common/FunctionCampLogo.png"
intro_file = "./assets/common/Intro.mov"


def add_watermark(clip):
    watermark = (
        ffmpeg.input("./assets/common/FunctionCampLogo.png", loop="1")
        .filter("fade", type="in", start_time=get_video_duration(intro_file), duration=0.1)
        .filter("scale", width="768", height="-1")
        .filter("colorchannelmixer", aa="0.5")
    )
    return clip.overlay(watermark, x=3000, y=1950, shortest=True)


def add_watermarks(clips):
    return [(add_watermark(video), audio) for (video, audio) in clips]


def prepend_intro(clips):
    intro = video_clip(intro_file)
    return video_concat_keep_audio([(intro.video, intro.audio)] + clips)


def write_audio_file(clip, output_file):
    clip.write_audiofile(output_file, codec="libmp3lame", bitrate="320k")


def create_video(content_clips, output_file):
    write_video_file(add_watermark(prepend_intro(content_clips)), output_file)
