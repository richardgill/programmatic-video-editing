# Programmatic Video Editing

https://www.youtube.com/watch?v=EMNmEe2Uf50

## Setup

### Prerequisites

- Python 3
- [Poetry](https://python-poetry.org/docs/#installation)

### Setup

`make install`

### Running

```
make preview_course timeline=videos/javascript/4_strings.py

make preview_course timeline=videos/javascript/4_strings.py write=true

make build_course timeline=videos/javascript/4_strings.py
```
