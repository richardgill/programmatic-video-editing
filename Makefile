# make preview_course timeline=videos/css/1.py outputFolder=./previews from=1 to=7
preview_course:
	poetry run python src/preview_course.py $(timeline) $(if $(from),--from=$(from),) $(if $(to),--to=$(to),) $(if $(outputFolder),--output-folder=$(outputFolder),) $(if $(write),--write=$(write),)

# make build_course timeline=videos/css/1.py outputFolder=./output
build_course:
	poetry run python src/build_course.py $(timeline) $(if $(outputFolder),--output-folder=$(outputFolder),)

install:
	poetry install
