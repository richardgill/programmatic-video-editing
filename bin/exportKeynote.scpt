on run argv
  tell application id "com.apple.systemevents" to tell process "Keynote"

    set frontmost to true
    tell menu bar 1 to tell menu bar item "File" to tell menu 1
      tell menu item "Export To" to tell menu 1 to click menu item "Movie…"
    end tell

    delay 0.1
    tell window 1 to tell sheet 1
      set value of 1st text field whose help is "Type how many seconds to wait between slides." to "1"
      set value of 1st text field whose help is "Type how many seconds to wait between builds." to "1"
      click (1st radio button whose title is "From:")
      keystroke (ASCII character 9) # tab

      keystroke item 2 of argv
      delay 0.1
      keystroke (ASCII character 9) # tab
      delay 0.1
      keystroke item 3 of argv

      tell (1st pop up button whose help is "Choose a size for the movie.")
        perform action "AXShowMenu"
        delay 0.1
        click menu item "Custom..." of menu 1
      end tell
      set value of 1st text field whose help is "Type the width you want for the movie." to "3840"
      set value of 1st text field whose help is "Type the height you want for the movie." to "2160"

      click (1st radio button whose title is "H.264")
      delay 0.1
      click button "Next…"

      delay 2
      keystroke item 1 of argv
      key code 76
    end tell

  end tell
end run
